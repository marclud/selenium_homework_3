from selenium.webdriver import Chrome
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class BasePage:
    def __init__(self, driver: Chrome):
        self.driver = driver

    def verify_is_element_on_page(self, element):
        WebDriverWait(self.driver, 5).until(expected_conditions.visibility_of_element_located(element))

    def verify_is_element_clickable_and_click_it(self, element):
        WebDriverWait(self.driver, 5).until(expected_conditions.element_to_be_clickable(element)).click()
