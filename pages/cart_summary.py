from selenium.webdriver.common.by import By

from pages.base import BasePage


class CartSummaryPage(BasePage):
    total_price_container = (By.ID, 'total_product')
    shipping_price_container = (By.ID, 'total_shipping')

    def check_products_price(self):
        self.verify_is_element_on_page(self.total_price_container)
        price = self.driver.find_element(*self.total_price_container).text
        return float(price.replace('$', ''))

    def check_shipping_price(self):
        price = self.driver.find_element(*self.shipping_price_container).text
        return float(price.replace('$', ''))
