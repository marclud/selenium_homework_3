from selenium.webdriver.common.by import By

from pages.base import BasePage


class HomePage(BasePage):
    search_box_selector = (By.ID, "search_query_top")
    submit_search_selector = (By.NAME, "submit_search")

    def search_product(self, product_name):
        self.verify_is_element_on_page(self.search_box_selector)
        self.driver.find_element(*self.search_box_selector).clear()
        self.driver.find_element(*self.search_box_selector).send_keys(product_name)
        self.driver.find_element(*self.submit_search_selector).click()
