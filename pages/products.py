from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By

from pages.base import BasePage


class ProductsPage(BasePage):
    product_container_selector = (By.CLASS_NAME, "product-container")
    add_to_cart_selector = (By.CSS_SELECTOR, ".ajax_add_to_cart_button")
    continue_shopping_selector = (By.CSS_SELECTOR, ".continue.button")
    shopping_cart_selector = (By.XPATH, "//*[@id='layer_cart']/div[1]/div[2]/div[4]/a")

    def add_element_to_cart(self):
        self.verify_is_element_on_page(self.product_container_selector)
        element = self.driver.find_element(*self.product_container_selector)
        hover = ActionChains(self.driver).move_to_element(element)
        hover.perform()
        self.driver.find_element(*self.add_to_cart_selector).click()

    def proceed_to_checkout(self):
        self.verify_is_element_clickable_and_click_it(self.shopping_cart_selector)
