from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import pytest


@pytest.fixture(scope='module')
def driver(request):

    BASE_URL = 'http://automationpractice.com/index.php'

    wd = webdriver.Chrome(executable_path=ChromeDriverManager().install())
    wd.get(BASE_URL)

    def close_driver():
        wd.quit()

    request.addfinalizer(close_driver)
    return wd
