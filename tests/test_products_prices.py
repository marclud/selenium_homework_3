from pages.home import HomePage
from pages.products import ProductsPage
from pages.cart_summary import CartSummaryPage


def test_checking_product_prices(driver):
    home_page = HomePage(driver)
    products_page = ProductsPage(driver)
    cart_summary_page = CartSummaryPage(driver)

    home_page.search_product('Dress')

    products_page.add_element_to_cart()
    products_page.proceed_to_checkout()

    product_price = cart_summary_page.check_products_price()
    assert product_price < 50.00

    shipping_price = cart_summary_page.check_shipping_price()
    assert shipping_price == 2.00
